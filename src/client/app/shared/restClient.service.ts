import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

import { HttpClientModule } from '@angular/common/http';
//import {Subject} from 'rxjs/Subject';
//import {BehaviorSubject} from 'rxjs/subject/BehaviorSubject';

//import {config} from '../../shared/smartadmin.config';
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
//import 'rxjs/add/observable/q';
import 'rxjs/add/operator/publishReplay';



@Injectable()
export class RestClientService {
  actionUrl : any;
  url: string;
  //BASE_URL= location.origin + '/api/ansut-volontariat-1.0/';
  // BASE_URL= "http://10.100.61.170:8080/";
  BASE_URL= "http://ip172-18-0-10-b9m8t569a8q000c9c2t0-8080.direct.labs.play-with-docker.com/spring-base-1.0/";

  //img_url: config.IMG_DECOUPES;

  /*let headers = new Headers({ 'Access-Control-Allow-Origin': '*' });
    let options = new RequestOptions({ headers: headers });*/

  constructor(private http: HttpClient) {}

  /*get(url,data):Observable<any> {
    return this.http.post(this.BASE_URL + url + '/getByCriteria',data).catch(this.handleError)
  }*/

  getByCriteria(action:any, data:any){
    this.actionUrl = this.BASE_URL + action;

        this.url = "/getByCriteria"
        return this.http.post(this.actionUrl + this.url,data).catch(this.handleError)

  }
  delete(action:any, data:any){
    this.actionUrl = this.BASE_URL + action;

        this.url = "/delete"
        return this.http.post(this.actionUrl + this.url,data).catch(this.handleError)

  }

  execute(methode:string,action:any, data:any){
    this.actionUrl = this.BASE_URL;
  return this.http.post(this.actionUrl + methode+'/' + action,data).catch(this.handleError)
  }

  login(action:any, data:any){
    this.actionUrl = this.BASE_URL + action;
    this.url = "/login"
    return this.http.post(this.actionUrl + this.url,data).catch(this.handleError)
  }
  resetPassword(action:any, data:any){
    this.actionUrl = this.BASE_URL + action;
    this.url = "/resetPassword"
    return this.http.post(this.actionUrl + this.url,data).catch(this.handleError)
  }
  forgotPassword(action:any, data:any){
    this.actionUrl = this.BASE_URL + action;
    this.url = "/forgotPassword"
    return this.http.post(this.actionUrl + this.url,data).catch(this.handleError)
  }
  forgotPasswordValidation(action:any, data:any){
    this.actionUrl = this.BASE_URL + action;
    this.url = "/forgotPasswordValidation"
    return this.http.post(this.actionUrl + this.url,data).catch(this.handleError)
  }

  restApiBase(action:any){
    this.actionUrl = this.BASE_URL + action;
    return {
      get(data:any){
        this.url = "/getByCriteria"
        return this.http.post(this.actionUrl + this.url,data).catch(this.handleError)
      },
      execute(data:any,methodeUrl:any){
        return this.http.post(this.actionUrl + '/' + methodeUrl,data).catch(this.handleError)
      }
    }
  }

  private handleError(error: any): Promise<any> {
     console.error('An error occurred', error); // for demo purposes only
     return Promise.reject(error.message || error);
  }

}
