import { NgModule } from '@angular/core';
import { ConnexionComponent } from './connexion.component';
import { ConnexionRoutingModule } from './connexion-routing.module';
import { SharedModule } from '../shared/shared.module';
import { NameListService } from '../shared/name-list/name-list.service';

@NgModule({
  imports: [ConnexionRoutingModule, SharedModule],
  declarations: [ConnexionComponent],
  exports: [ConnexionComponent],
  providers: [NameListService]
})
export class ConnexionModule { }
