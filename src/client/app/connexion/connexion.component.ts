import { Component, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NameListService } from '../shared/name-list/name-list.service';
import { RestClientService } from '../shared/restClient.service';
import { RouterModule,Router } from '@angular/router';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-connexion',
  templateUrl: 'connexion.component.html',
  styleUrls: ['connexion.component.css'],
  providers: [RestClientService]
})
export class ConnexionComponent implements OnInit {

  newName = '';
  errorMessage: string;
  errorRetour: any;
  listeMembres: any[] = [];
  names: any[] = [];
  user = new User();
  /**
   * Creates an instance of the HomeComponent with the injected
   * NameListService.
   *
   * @param {NameListService} nameListService - The injected NameListService.
   */
  constructor(public nameListService: NameListService,private restClient: RestClientService,
  private router : Router) {

  }

  /**
   * Get the names OnInit
   */
  ngOnInit() {
    this.getNames();
    //this.getMembres();
  }

  /**
   * Handle the nameListService observable
   */
  getNames() {
    this.nameListService.get()
      .subscribe(
        names => this.names = names,
        error => this.errorMessage = <any>error
      );
  }

  createMembre(user:any){
    var methode = "user";
    var data = {
      "datasUser":[
          {
            "nom": user.nom,
            "prenoms": user.prenoms,
            "userName": user.userName,
            "email":  user.email
          }]
    }
    console.log("ok data to send", data);
    //return;
    this.restClient.execute(methode,"create",data).subscribe(res=>{
    console.log('retour de requete',res);
    /*  if(res && !res.hasError){
        console.log("operation réussi avec succes");
      }else{
        console.error("erreur");
      }*/
    })
  }


  login(user:any){
    var methode = "user";
    var data = {
      dataUser :user
    }
    console.log('user login data',data);
    //return;
    this.restClient.login(methode,data).subscribe(res=>{
      console.log('ok get res',res);

       if(res && !res.hasError){
         this.user = new User();
         this.router.navigate(["home"]);
          console.log('ok get res',res);
        }else{
             this.errorRetour = res ? res.status.message : null;
        }
    });
  }
  resetPassword(user:any){
    var methode = "user";
    var data = {
      dataUser :user
    }
    this.restClient.login(methode,data).subscribe(res=>{
      console.log('ok get res',res);
       if(res && !res.hasError){
          //this.listeMembres = res ? res.items : [];
          console.log('ok get res',res);
        }else{

        }
    });
  }
  /**
   * Pushes a new name onto the names array
   * @return {boolean} false to prevent default form submit behavior to refresh the page.
   */
  addName(): boolean {
    // TODO: implement nameListService.post
    this.names.push(this.newName);
    this.newName = '';
    return false;
  }

}


class User{
  userName: string;
  password: string;
}
