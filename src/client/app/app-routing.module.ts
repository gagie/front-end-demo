import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ConnexionComponent } from './connexion/connexion.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      // define app module routes here, e.g., to lazily load a module
        // (do not place feature module routes here, use an own -routing.module.ts in the feature instead)


    ])
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

/*export const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    data: {pageTitle: 'Dashboard'},
    children: [
      {path: '', redirectTo: 'connexion', pathMatch: 'full'},
    {path: 'showroom', component: MainLayoutComponent,loadChildren: 'app/showroom/showroom.module#ShowroomModule',data:{pageTitle: 'Showroom'}},
      {path: 'dashboard',component: MainLayoutComponent, loadChildren: 'app/dashboard/dashboard.module#DashboardModule',data:{pageTitle: 'Dashboard'}},

  {path: '**', redirectTo: 'connexion'}
]}
//
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {useHash: true});*/
