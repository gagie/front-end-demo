import { Component, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NameListService } from '../shared/name-list/name-list.service';
import { RestClientService } from '../shared/restClient.service';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css'],
  providers: [RestClientService]
})
export class HomeComponent implements OnInit {

  newName = '';
  errorMessage: string;
  errorRetour: any;
  listeMembres: any[] = [];
  names: any[] = [];
  membre = new Membre();
  /**
   * Creates an instance of the HomeComponent with the injected
   * NameListService.
   *
   * @param {NameListService} nameListService - The injected NameListService.
   */
  constructor(public nameListService: NameListService,private restClient: RestClientService) {

  }

  /**
   * Get the names OnInit
   */
  ngOnInit() {
    this.getNames();
    this.getMembres();
  }

  /**
   * Handle the nameListService observable
   */
  getNames() {
    this.nameListService.get()
      .subscribe(
        names => this.names = names,
        error => this.errorMessage = <any>error
      );
  }

  createMembre(membre:any){
    var methode = "user";
    var data = {
      "datasUser":[
          {
            "id" : membre.id ? membre.id : null,
            "nom": membre.nom,
            "prenoms": membre.prenoms,
            "userName": membre.userName,
            "email":  membre.email
          }]
    }
    console.log("ok data to send", data);
    //return;
    this.restClient.execute(methode,!membre.id?"create" : "update",data).subscribe(res=>{
    console.log('retour de requete',res);
      if(res && !res.hasError){
        this.membre = new Membre();
        this.errorRetour = null;
        console.log("operation réussi avec succes");
        this.getMembres();
      }else{
        this.errorRetour = res ? res.status.message :  null;
        console.log("erreur");
      }
    })
  }


  getMembres(){
    var methode = "user";
    var data = {
      dataUser :{}
    }
    this.restClient.getByCriteria(methode,data).subscribe(res=>{
      console.log('ok get res',res);
        if(res && !res.hasError){
          this.listeMembres = res ? res.itemsUser : [];
          console.log('ok get res',res);
        }else{

        }
    });
  }

  modifier(item:any){
    console.log('item',item);
    this.membre = Object.assign({},item);
  }

  delete(user:any){
    var methode = "user";
    var data = {
      datasUser :[{
        id : user.id
      }]
    }
    console.log("data to delete",data);
    this.restClient.delete(methode,data).subscribe(res=>{
      console.log('ok get res',res);
        if(res && !res.hasError){
          this.getMembres();
          //this.listeMembres = res ? res.itemsUser : [];
          console.log('ok get res',res);
        }else{

        }
    });
  }



  /**
   * Pushes a new name onto the names array
   * @return {boolean} false to prevent default form submit behavior to refresh the page.
   */
  addName(): boolean {
    // TODO: implement nameListService.post
    this.names.push(this.newName);
    this.newName = '';
    return false;
  }

}


class Membre{
  id: any;
  nom : string;
  prenoms : string;
  userName: string;
  email: string;
  password: string;
}
